# Die CI/CD Konfigurationsdatei

Die Konfigurationsdatei bestimmt,

- das **Betriebssystem**, in dem sie ausgeführt wird
- **wann** die Pipeline ausgeführt wird
- unter welchen **Bedingungen** sie ausgeführt wird
- die Stadien (_Stages_), die sie durchläuft und die _Jobs_, die während einer _Stage_ ausgeführt werden
- welche einzelnen **Befehle** ausgeführt werden

Beispiele:

```yaml
image: ubuntu:22.04

hello:
  script:
    - echo "Good morning, class!"
```

```yaml
image: ubuntu:22.04

stages:
  - greet
  - leave

hello:
  stage: greet
  script:
    - echo "Good morning, class!"

bye:
  stage: leave
  script:
    - echo "Good bye, class!"
```

- Jobs sind die minimale zusammenhängende Einheit (`hello`)
- Jobs können in Stages zusammengefasst werden (`greet` & `leave`)
- Dateien, die in einem Job heruntergeladen oder erzeugt werden, stehen **nicht** in anderen Jobs zur Verfügung
- Erfahrung mit Linux-Commandline ist **notwendig**
